import React from 'react';
import { Formik, Field, Form, ErrorMessage ,} from 'formik';
import * as Yup from 'yup';
import "../login.css"
const Login = () => {

  return (
    <Formik
      initialValues={{ email: '', password: '' }}
      validationSchema={Yup.object({
        email:Yup.string().email('Invalid email address').required('Email feild if Required'),
        password: Yup.string().min(10,"Must be 10 characters or more")
          .max(12, 'not more than 12 characters')
          .matches(/(\d)/, 'must contain one number')
          .matches(/(\W)/, 'must contain one special character')
          .required('Password if Required'),
      })}
      onSubmit={(values, { setSubmitting }) => {
        setTimeout(() => {
          alert(JSON.stringify(values, null, 2));
          setSubmitting(false);
        }, 400);
      }}
    >
     {({ errors, touched }) => (
      <Form className="login-form">
      <div className='input-container'>
        <label className="input-lable" htmlFor="email">Email</label>
        {/* <EmailIcon /> */}
        <Field className="login-email login-input" placeholder="Username" name="email" type="email" />
          {
            errors.email && touched.email ? <p className="error"> 
            <ErrorMessage className='error' name="email" />
        </p> : null
          }  
        </div>

        <div className='input-container'>
            <label className="input-lable" htmlFor="password">Password</label>
            <Field className="login-password login-input" placeholder="password" name="password" type="text" />
            {
            errors.password && touched.password ? <p className="error"> 
            <ErrorMessage className='error' name="password" />
        </p> : null
          }  
        </div>

        <button className="submit-button" type="submit">Submit</button>
      </Form>
      )}

    </Formik>
  );
};
export default Login;